package main

import (
	"fmt"
	"gitlab.com/ady.xcloud7/golangforlife/introduce/cetak"
	"gitlab.com/ady.xcloud7/golangforlife/introduce/pointer"
)

func main() {
	var a string = "nama"
	b := 10
	cetak.Cetak(b, a)
	d := cetak.Indra("dana cukup", "diduain")
	fmt.Printf("%s \n", d)

	_, z := cetak.Ady()
	fmt.Println("Istri Ady : " + z)

	s := cetak.Yana(1, 2, 3, 4, 5)
	fmt.Printf("Hasil rata2 : %f", s)

	pointer.Pointer()
}
