package cetak

import "fmt"

func Cetak(x int, y string) {
	fmt.Printf("Hello World %s %d \n", y, x)
}

func Ady() (string, string) {
	return "istri 1", "istri 2"
}

func Yana(n ...int) float32 {
	var total int
	for _, x := range n {
		total += x
	}

	p := float32(total / len(n))
	return p
}
